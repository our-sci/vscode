## Features

The Our-Sci extension allows developers to fetch Surveys directly and have syntax completion for survey questions and CSV field sets.

## Requirements

This extension will create a `.oursci-surveys` folder and requires a measurement-script or dashboard project to make full use of the features.

Use  `Shift + Ctrl + P`  then `Select Survey` to get started pulling Survey prototypes and results.

![Survey Selection](https://gitlab.com/our-sci/vscode/raw/master/screen.png)


## Extension Settings

Currently no settings are supported, this is a work in progress.

## Known Issues

No known issues.

## Release Notes

### 1.0.0

Initial release